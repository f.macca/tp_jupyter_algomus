# Pour TP
jupyter==1.0.0
matplotlib==3.3.2
music21==6.7.1
numpy==1.18.5
pandas==1.1.3
seaborn==0.11.0
tqdm==4.51.0

# =========================
# Pour présentation
plotly==4.14.3

# Pour installer RISE (Slides)
# -- Windows --
# pip install rise
# jupyter-nbextension install rise --py --sys-prefix
# jupyter-nbextension enable rise --py --sys-prefix
# 
# -- Linux --
# pip install rise
# sudo -E env "PATH=$PATH" jupyter-nbextension install rise --py
# sudo -E env "PATH=$PATH" jupyter-nbextension enable rise --py

